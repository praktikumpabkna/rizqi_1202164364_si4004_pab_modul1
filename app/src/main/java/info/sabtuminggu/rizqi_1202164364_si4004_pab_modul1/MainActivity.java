package info.sabtuminggu.rizqi_1202164364_si4004_pab_modul1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText edt_alas, edt_tinggi;
    Button btn_cek;
    TextView tv_luas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edt_alas = findViewById(R.id.edt_alas);
        edt_tinggi = findViewById(R.id.edt_tinggi);
        btn_cek = findViewById(R.id.btn_cek);
        tv_luas = findViewById(R.id.tv_luas);

        btn_cek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    int alas = Integer.parseInt(edt_alas.getText().toString());
                    int tinggi = Integer.parseInt(edt_tinggi.getText().toString());
                    int luas = alas*tinggi;
                    tv_luas.setText(Integer.toString(luas));
                }catch (Exception e){
                    tv_luas.setText("Pasti ada kesalahan Coba cek lagi");
                    Toast.makeText(MainActivity.this,
                            e.toString(), Toast.LENGTH_LONG).show();
                }
            }
        });


    }
}
